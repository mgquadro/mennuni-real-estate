import resolveConfig from 'tailwindcss/resolveConfig'

const tailwindConfig = resolveConfig(require('./tailwind.config.js'))

require('dotenv').config()

export default {
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  mode: 'universal',

  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: 'server', // SSR
  /*
   ** Netlify
   ** 1) target: 'static'
   ** 2) Disable csp (https://github.com/nuxt/nuxt.js/issues/6592)
   ** 3) Uncomment sitemap.hostname
   */
  target: 'static',

  // https://nuxtjs.org/api/configuration-modern
  modern: true,

  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || '',
      },
    ],
    link: [
      {
        rel: 'preconnect',
        href: process.env.BACKEND,
        crossorigin: true,
      },
      {
        rel: 'dns-prefetch',
        href: process.env.BACKEND,
        crossorigin: true,
      },
      {
        rel: 'preconnect',
        href: 'https://cdn.jsdelivr.net',
        crossorigin: true,
      },
      {
        rel: 'dns-prefetch',
        href: 'https://cdn.jsdelivr.net',
        crossorigin: true,
      },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css2?family=Petrona:wght@400;700&display=swap',
      },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css2?family=IBM+Plex+Sans+Condensed:wght@400;700&display=swap',
      }
    ],
  },

  /*
   ** Global CSS
   */
  css: [],

  /*
   ** Customize the progress-bar color
   */
  // loading: {
  //   color: tailwindConfig.theme.colors['white'],
  //   height: '0.25rem',
  // },

  pageTransition: {
    name: 'fade',
    mode: 'out-in',
  },

  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: [
    '~plugins/modernizr.client.js',
    '~plugins/filters.js',
    // Doc: https://github.com/ankurk91/vue-loading-overlay
    '~plugins/vue-loading-overlay.js',
    '~plugins/vue-js-modal.js',
    '~plugins/vue-form.js',
  ],

  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true, // Watch too slow: https://github.com/nuxt/components/issues/18
  // components: [{ path: '~/components', global: 'dev' }],

  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/dotenv-module
    '@nuxtjs/dotenv',
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://github.com/nuxt-community/gtm-module
    '@nuxtjs/gtm',
    // Doc: https://github.com/aceforth/nuxt-optimized-images
    '@aceforth/nuxt-optimized-images',
    // Doc: https://pwa.nuxtjs.org/
    '@nuxtjs/pwa',
    // Doc: https://github.comnuxt-community/stylelint-module
    '@nuxtjs/stylelint-module',
    // Doc: https://github.com/nuxt-ommunity/nuxt-tailwindcss
    '@nuxtjs/tailwindcss',
  ],

  eslint: {
    fix: true,
  },

  gtm: {
    // enabled: true, // enable in dev
    id: process.env.GTM_KEY,
    pageTracking: true,
  },

  optimizedImages: {
    optimizeImages: true,
    // optimizeImagesInDev: true,
    mozjpeg: {
      quality: 95,
    },
  },

  // https://pwa.nuxtjs.org/modules/workbox.html
  workbox: {
    cacheId: process.env.npm_package_name,
    offlineGoogleAnalytics: true,
    runtimeCaching: [
      {
        urlPattern: '.*(?:googleapis|gstatic).com.*',
        handler: 'staleWhileRevalidate',
        method: 'GET',
        strategyOptions: {
          cacheName: 'workbox-runtime - google',
          cacheableResponse: {
            statuses: [0, 200],
          },
        },
      },
      {
        urlPattern: process.env.BACKEND,
        handler: 'networkFirst',
        method: 'GET',
        strategyOptions: {
          cacheName: 'backend',
          cacheableResponse: {
            statuses: [0, 200],
          },
        },
      },
    ],
  },
  // https://pwa.nuxtjs.org/modules/meta.html
  meta: {
    mobileAppIOS: true,
  },
  // https://pwa.nuxtjs.org/modules/icon.html
  icon: {
    /* icon options */
  },
  // https://pwa.nuxtjs.org/modules/manifest.html
  manifest: {
    name: process.env.npm_package_description,
    short_name: process.env.npm_package_name,
    lang: 'it-IT',
    background_color: tailwindConfig.theme.colors.white,
    theme_color: tailwindConfig.theme.colors.white,
    // display: 'browser'
  },

  // https://github.com/nuxt-community/tailwindcss-module#referencing-in-javascript
  tailwindcss: {
    // exposeConfig: true,
  },

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    // '@nuxtjs/axios',
    // Doc: https://dev.auth.nuxtjs.org
    // '@nuxtjs/auth-next',
    // Doc: https://i18n.nuxtjs.org
    // 'nuxt-i18n',
    // Doc: https://github.com/nuxt-community/device-module
    // '@nuxtjs/device',
    // Doc: https://github.com/nuxt/content
    '@nuxt/content',
    // Doc: https://github.com/nuxt-community/sitemap-module
    '@nuxtjs/sitemap', // Always the last
  ],

  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  // axios: {
  //   baseURL: process.env.BACKEND,
  // },

  /*
   ** Auth module with laravel/jwt configuration
   ** See https://dev.auth.nuxtjs.org/providers/laravel-jwt
   */
  // auth: {
  //   redirect: {
  //     login: '/login',
  //     home: '/profile',
  //   },
  //   strategies: {
  //     laravelJWT: {
  //       provider: 'laravel/jwt',
  //       url: process.env.BACKEND,
  //       token: {
  //         property: 'access_token',
  //         maxAge: 60 * 60,
  //       },
  //       refreshToken: {
  //         maxAge: 20160 * 60,
  //       },
  //     },
  //   },
  // },

  /*
   ** nuxt-i18n configuration
   ** See https://i18n.nuxtjs.org/options-reference.html
   */
  // i18n: {
  //   vueI18n: {
  //     fallbackLocale: 'en',
  //   },
  //   vueI18nLoader: true,
  //   locales: [
  //     {
  //       code: 'it',
  //       iso: 'it-IT',
  //       file: 'it-IT.js',
  //     },
  //     {
  //       code: 'en',
  //       iso: 'en-GB',
  //       file: 'en-GB.js',
  //     },
  //   ],
  //   defaultLocale: 'it',
  //   strategy: 'prefix_except_default',
  //   vuex: {
  //     syncLocale: true,
  //     syncRouteParams: true,
  //   },
  //   parsePages: true,
  // },

  /*
   ** Content module configuration
   ** See https://content.nuxtjs.org/configuration
   */
  content: {},

  sitemap: {
    hostname: 'https://mennunirealestate.com', // Required for static
    // i18n: true,
  },

  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {
    // https://nuxtjs.org/api/configuration-build/#extractcss
    extractCSS: true,
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
          options: {
            fix: true,
          },
        })
      }
    },
  },

  render: {
    http2: {
      push: true,
    },
    static: {
      maxAge: 1000 * 60 * 60 * 24 * 7,
    },
    crossorigin: 'use-credentials',
    // csp: true,
    // https://nuxtjs.org/api/configuration-render/#csp
    // csp: {
    //   reportOnly: true,
    //   hashAlgorithm: 'sha256',
    //   policies: {
    //     'default-src': ["'self'"],
    //     'script-src': [
    //       "'self'",
    //       "'unsafe-inline'",
    //       "'unsafe-eval'", // vue-form
    //       // https://developers.google.com/tag-manager/web/csp
    //       'https://www.googletagmanager.com',
    //       'https://www.google-analytics.com',
    //       'https://ssl.google-analytics.com',
    //       // https://developers.google.com/recaptcha/docs/faq#im-using-content-security-policy-csp-on-my-website.-how-can-i-configure-it-to-work-with-recaptcha
    //       'https://www.google.com/recaptcha/',
    //       'https://www.gstatic.com/recaptcha/',
    //     ],
    //     'img-src': [
    //       "'self'",
    //       'data:',
    //       'blob:',
    //       // https://developers.google.com/tag-manager/web/csp
    //       'https://www.googletagmanager.com',
    //       'https://ssl.gstatic.com',
    //       'https://www.gstatic.com',
    //       'https://www.google-analytics.com',
    //       process.env.BACKEND,
    //     ],
    //     'style-src': [
    //       "'self'",
    //       "'unsafe-inline'",
    //       // https://developers.google.com/tag-manager/web/csp
    //       'https://tagmanager.google.com',
    //       'https://fonts.googleapis.com',
    //     ],
    //     'font-src': ["'self'", 'data:', 'https://fonts.gstatic.com'],
    //     'frame-src': ["'self'", 'https://www.google.com/recaptcha/'],
    //     'connect-src': [
    //       "'self'",
    //       // https://developers.google.com/tag-manager/web/csp
    //       'https://www.google-analytics.com',
    //       process.env.BACKEND,
    //     ],
    //     'object-src': ["'none'"],
    //   },
    //   // unsafeInlineCompatibility: true,
    //   // addMeta: true,
    // },
  },

  server: {
    port: process.env.PORT,
  },
}
