export const state = () => ({
  menu: [],
})

export const mutations = {
  setMenu(state, menu) {
    state.menu = menu
  },
}

export const actions = {
  async nuxtServerInit({ state, commit, dispatch }, { $content, app }) {
    const pages = await $content(app.i18n.locale)
      .where({ menu: true })
      .sortBy('position')
      .fetch()
    commit('setMenu', pages)
  },
}
