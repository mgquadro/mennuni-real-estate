// Default config: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js

module.exports = {
  theme: {
    // screens: {
    //   sm: '640px',
    //   md: '768px',
    //   lg: '1024px',
    //   xl: '1280px',
    // },
    extend: {
      colors: {
        primary: '#2F495E',
        secondary: '#FFFFFF',
        'red-mennuni': '#951F1F',
        'red-mennuni-scuro': '#5F0202',
        'gray-mennuni-scuro': '#434343',
      },
      height: {
      },
    },
    fontFamily: {
      sans: "'IBM Plex Sans Condensed', sans-serif;",
      serif: "'Petrona', serif;"
    },
  },
  variants: {},
  plugins: [
    // Doc: https://github.com/tailwindcss/typography
    // require('@tailwindcss/typography'),
    require('./plugins/tailwind/grid')({
      columns: 12,
      // margin: '2rem',
      margins: {
        default: '1rem',
        sm: '2rem',
        xl: '3rem',
      },
      gutter: '2rem',
      maxWidth: '1520px',
    }),
  ],
  purge: {
    // https://tailwindcss.com/docs/controlling-file-size/#removing-unused-css
    // enabled: true, // Enable in dev
    enabled: process.env.NODE_ENV === 'production',
    content: [
      'components/**/*.vue',
      'layouts/**/*.vue',
      'pages/**/*.vue',
      'plugins/**/*.js',
      'nuxt.config.js',
    ],
    // options: {
    //   whitelist: ['body', 'html', 'nuxt-progress', /-leave/, /-enter/, /v--modal/]
    // },
  },
}
