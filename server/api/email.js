/* eslint-disable */

const { Router } = require('express')
const axios = require('axios')
const bodyParser = require('body-parser')
const nodemailer = require('nodemailer')
const multer = require('multer')
const xssFilters = require('xss-filters')

const Email = require('email-templates')

const config = require('../config')

const env = process.env.NODE_ENV || 'development'
const router = Router()
let statusResponse = null

const upload = multer()

// Body Parser Middleware
router.use(bodyParser.urlencoded({ extended: false }))
router.use(bodyParser.json())

// Nodemailer POST
router.post('/email', upload.none(), (req, res) => {
  const formData = req.body
  const recaptchaResponse = formData['token']
  axios
    .post(
      `https://www.google.com/recaptcha/api/siteverify?secret=${process.env.RECAPTCHA_SECRET}&response=${recaptchaResponse}`
    )
    .then(function ({ data }) {
      if (data.success) {
        const name = xssFilters.inHTMLData(formData.name).split(' ')[0]
        const mailTitle = `Richiesta inviata da ${name}`
        let mailContent =
          '<h3>Dettaglio Contatto</h3>' + '<ul>' + `<li>Nome: ${name}</li>`
        if (formData.surname) {
          mailContent += `<li>Cognome: ${xssFilters.inHTMLData(formData.surname)}</li>`
        }
        if (formData.email) {
          mailContent += `<li>Email: ${xssFilters.inHTMLData(formData.email)}</li>`
        }
        if (formData.phone) {
          mailContent += `<li>Telefono: ${xssFilters.inHTMLData(formData.phone)}</li>`
        }
        if (formData.newsletter) {
          mailContent += `<li>Newsletter: ${formData.newsletter ? 'Sì' : 'No'}</li>`
        }
        mailContent += '</ul>'
        if (formData.message) {
          mailContent +=
            '<h3>Messaggio</h3>' +
            `<p>${xssFilters.inHTMLData(formData.message)}</p>`
        }

        const responseTitle = 'Richiesta inviata!'
        const responseContent = `Grazie per averci contattato.
        <br><br>
        Il nostro staff ti risponderà non appena possibile.`

        const transport = nodemailer.createTransport(config.transporter)

        const email = new Email({
          views: { root: __dirname },
          message: {
            from: config.transporter.auth.user
          },
          send: true,
          // send: env === 'production',
          transport
        })

        email
          .send({
            template: 'email-template',
            message: {
              to: config.to,
              bcc: config.bcc,
              replyTo: formData.email,
              subject: mailTitle
            },
            subjectPrefix:
              env === 'production' ? false : `[${env.toUpperCase()}] `,
            locals: {
              title: mailTitle,
              content: mailContent
            }
          })
          .then(() => {
            email
              .send({
                template: 'email-template',
                message: {
                  to: formData.email,
                  bcc: config.bcc,
                  replyTo: config.to,
                  subject: responseTitle
                },
                subjectPrefix:
                  env === 'production' ? false : `[${env.toUpperCase()}] `,
                locals: {
                  title: responseTitle,
                  content: responseContent
                }
              })
              .then(okStatus)
              .catch(err => errorStatus(err))
          })
          .catch(err => errorStatus(err))
        if (formData.newsletter) {
          axios({
            method: 'post',
            url: `${process.env.MAILCHIMP_API}lists/${process.env.MAILCHIMP_LIST}/members/`,
            auth: {
              username: 'mgquadro',
              password: process.env.MAILCHIMP_KEY
            },
            data: {
              email_address: formData.email,
              status: 'subscribed',
              merge_fields: {
                NAME: formData.name,
                LOCATION: formData.locationName,
                PHONE: formData.phone
              }
            }
          })
            .then(res => {
              console.log(res)
            })
            .catch(err => errorStatus(err))
        }
      } else {
        errorStatus(data['error-codes'])
      }
    })
    .catch(err => errorStatus(err))

  const okStatus = function () {
    console.log('Mail sent')
    statusResponse = 200
    res.status(statusResponse).json({ message: 'Mail sent' })
  }

  const errorStatus = function (error) {
    console.error(error)
    statusResponse = 500
    res.status(statusResponse)
  }
})



// GET notifications
router.get('/email', (req, res) => {
  res.send(statusResponse)
})

module.exports = router
