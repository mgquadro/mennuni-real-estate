const { Router } = require('express')

const email = require('./email')

const router = Router()

router.use(email)

module.exports = router
