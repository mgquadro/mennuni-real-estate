const config = {
  transporter: {
    host: 'mgquadro.com',
    port: 25,
    secure: false, // upgrade later with STARTTLS
    tls: {
      rejectUnauthorized: false
    },
    auth: {
      user: '',
      pass: ''
    }
  },
  to: '',
  bcc: ''
}

module.exports = config
