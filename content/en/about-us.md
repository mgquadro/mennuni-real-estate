---
name: about-us
title: About us
description: Descrizione Pagina Chi siamo
header: /img/index.jpg
menu: true
position: 6
---

Mennuni Real Estate has been dealing in real estate brokerage for over 35 years, it performs sales and rentals of residential properties, offices, commercial spaces, industrial buildings and hotels. Know-how that requires professionalism, competence, seriousness and discretion.

"We are proud of having been able to satisfy the most demanding customers, the future sees us in the constant and patient search for perfection in the quality of our services"
Alessandro Mennuni

Mennuni RE is a family business founded in 1983 surrounded by expert consultants that deals with brokerage in the sale and rental of properties of any intended use throughout the country.
Over the years it has achieved excellent successes both in Italy and abroad, always trying to perfect itself in meeting customers requirements.
