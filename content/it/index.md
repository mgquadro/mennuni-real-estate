---
name: index
title: Mennuni Real Estate
menuTitle: Home
description: Mennuni Real Estate
header: /img/index.jpg
menu: true
position: 1
---

<span class="block px-8 mb-4 font-sans text-2xl italic md:px-0">
Siamo orgogliosi di un passato in cui abbiamo potuto soddisfare la clientela più esigente, <br>il futuro ci vede alla ricerca costante e paziente della perfezione nella qualità dei nostri servizi.
</span>
