---
name: chi-siamo
title: Chi siamo
description: Descrizione Pagina Chi siamo
header: /img/index.jpg
menu: true
position: 2
---

La Mennuni Real Estate da oltre 35 anni nell’intermediazione immobiliare, svolge attività di compravendita e locazione di abitazioni, uffici, spazi commerciali, immobili industriali e strutture ricettive. Esperienze che richiedono professionalità, competenza, serietà e discrezione.

<span class="bg-gray-300">“Siamo orgogliosi di un passato in cui abbiamo potuto soddisfare la clientela più esigente, il futuro ci vede alla ricerca costante e paziente della perfezione nella qualità dei nostri servizi”.</span > <span>Alessandro Mennuni</span>

La Mennuni RE è una azienda familiare nata nel 1983 che si avvale di consulenti esperti e si occupa di intermediazione in compravendite e locazioni di immobili di qualsiasi destinazioni d’uso su tutto il territorio nazionale.

Negli anni ha raggiunto ottimi successi sia in Italia che all’estero, nel costante obiettivo di perfezionarsi nell’adempiere le esigenze dei propri clienti.