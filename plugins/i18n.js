export default function ({ store, app, $content }) {
  // beforeLanguageSwitch called right before setting a new locale
  app.i18n.beforeLanguageSwitch = (oldLocale, newLocale) => {
    // console.log(oldLocale, newLocale)
  }
  // onLanguageSwitched called right after a new locale has been set
  app.i18n.onLanguageSwitched = async (oldLocale, newLocale) => {
    const pages = await $content(app.i18n.locale)
      .where({ menu: true })
      .sortBy('position')
      .fetch()
    store.commit('setMenu', pages)
  }
}
