/* eslint-disable */

// .wrapper, container with max width
// .row, element with flex & wrap
// .col, automatic columns occupied by the element
// .col-{size}, specific number of columns occupied by the element
// .order-first, element first in the row
// .order-last, element last in the row
// .order-{size}, element at specific place in the row
// .offset-{size}, number of columns of offset for the element

const _ = require('lodash')

module.exports = function ({
  columns = 12,
  margin = 0,
  margins = {
    default: margin,
  },
  gutter = 0,
  maxWidth = null,
  variants = ['responsive'],
}) {
  return function ({ e, addComponents, theme }) {
    const columnsArray = _.range(1, columns + 1)
    const screens = theme('screens', {})
    maxWidth = maxWidth || screens.xl

    const responsiveWrapper = _.map(screens, (value, key) => {
      if (!margins.hasOwnProperty(key)) {
        return false
      }
      return {
        [`@media (min-width: ${value})`]: {
          '.wrapper': {
            maxWidth: `calc(2 * ${margins[key]} + ${maxWidth})`,
            paddingLeft: `${margins[key]}`,
            paddingRight: `${margins[key]}`,
          },
        },
      }
    })

    addComponents(
      [
        {
          '.wrapper': {
            width: '100%',
            maxWidth: `calc(2 * ${margins.default} + ${maxWidth})`,
            margin: '0 auto',
            paddingLeft: `${margins.default}`,
            paddingRight: `${margins.default}`,
          },
        },

        ...responsiveWrapper,

        {
          '.row': {
            display: 'flex',
            flexWrap: 'wrap',
            marginLeft: `calc(-${gutter} / 2)`,
            marginRight: `calc(-${gutter} / 2)`,
          },
        },

        {
          '.col': {
            flexBasis: 0,
            flexGrow: 1,
            maxWidth: '100%',
            paddingRight: `calc(${gutter} / 2)`,
            paddingLeft: `calc(${gutter} / 2)`,
          },
        },

        ...columnsArray.map((size) => ({
          [`.col-${size}`]: {
            position: 'relative',
            width: '100%',
            minHeight: '1px',
            paddingRight: `calc(${gutter} / 2)`,
            paddingLeft: `calc(${gutter} / 2)`,
            flex: `0 0 ${(size / columns) * 100}%`,
            maxWidth: `${(size / columns) * 100}%`,
          },
        })),

        {
          '.order-first': {
            order: '-1',
          },
        },

        {
          '.order-last': {
            order: `${columns + 1}`,
          },
        },

        ...columnsArray.map((size) => ({
          [`.order-${size}`]: {
            order: `${size}`,
          },
        })),

        ...columnsArray.map((size) => ({
          [`.offset-${size}`]: {
            marginLeft: getOffset(size, columns),
          },
        })),
      ],
      variants
    )
  }
}

function getOffset(size, columns) {
  const num = Math.min(size / columns, 1)
  return `${num * 100}%`
}
