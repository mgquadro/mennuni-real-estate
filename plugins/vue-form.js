import Vue from 'vue'

import VueForm from 'vue-form'

import { VueReCaptcha } from 'vue-recaptcha-v3'

Vue.use(VueForm, {
  validators: {
    // https://jsfiddle.net/fergal_doyle/9rn5kLkw/
    matches(value, attrValue) {
      if (!attrValue) {
        return true
      }
      return value === attrValue
    },
    // 'password-strength'(value) {
    //   return /(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/.test(value);
    // },

    word(value) {
      return value.match('^[A-Za-z\\s]{2,}$')
    },
    phone(value) {
      return value.match('^([+]\\d{2})?[\\d\\s-.]{7,15}$')
    },
  },
})

Vue.use(VueReCaptcha, {
  siteKey: process.env.RECAPTCHA_PUBLIC,
  loaderOptions: {
    autoHideBadge: true,
  },
})
