import Vue from 'vue'

export default ({ app, $moment }) => {
  Vue.filter('date', (value, format) => $moment(value).format(format))

  Vue.filter('locale', (value) => value[app.i18n.locale])

  Vue.filter('fullName', (value) => value.name + ' ' + value.surname)
}
