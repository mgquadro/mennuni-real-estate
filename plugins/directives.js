import Vue from 'vue'
import { gsap } from 'gsap'

let sticky
Vue.directive('sticky', {
  bind(el, binding) {
    const threshold = binding.value ? binding.value.threshold || 0 : 0
    const duration = binding.value ? binding.value.duration || 0.5 : 0.5
    const heightFrom = el.offsetHeight
    const heightTo = binding.value
      ? binding.value.height || heightFrom
      : heightFrom
    const zIndexTo = binding.value ? binding.value.zIndex || null : null
    const backgroundColorFrom = document.defaultView
      .getComputedStyle(el, null)
      .getPropertyValue('background-color')
    const backgroundImageFrom = document.defaultView
      .getComputedStyle(el, null)
      .getPropertyValue('background-image')
    const backgroundColorTo = binding.value
      ? binding.value.backgroundColor || backgroundColorFrom
      : backgroundColorFrom
    const backgroundImageTo = binding.value
      ? binding.value.backgroundImage || backgroundImageFrom
      : backgroundImageFrom
    const $ = e => document.querySelector(e)
    const tag =
      !window.chrome && 'WebkitAppearance' in document.documentElement.style
        ? 'body'
        : 'html'
    let stickified = false
    let animation
    sticky = () => {
      if ($(tag).scrollTop <= 0 && stickified) {
        stickified = false
        if (animation) {
          animation.kill()
        }
        animation = gsap.to(el, {
          duration,
          clearProps: 'all',
          height: heightFrom,
          backgroundColor: backgroundColorFrom,
          backgroundImage: backgroundImageFrom,
          ease: 'sine.out'
        })
      } else if ($(tag).scrollTop > threshold && !stickified) {
        stickified = true
        if (animation) {
          animation.kill()
        }
        if (threshold > 0) {
          animation = gsap.fromTo(
            el,
            {
              position: 'fixed',
              top: -heightFrom,
              backgroundColor: backgroundColorTo,
              backgroundImage: backgroundImageTo
            },
            {
              duration,
              top: 0,
              height: heightTo,
              zIndex: zIndexTo,
              ease: 'sine.out'
            }
          )
        } else {
          animation = gsap.fromTo(
            el,
            {
              position: 'fixed',
              backgroundColor: backgroundColorFrom,
              backgroundImage: backgroundImageFrom
            },
            {
              duration,
              height: heightTo,
              backgroundColor: backgroundColorTo,
              backgroundImage: backgroundImageTo,
              zIndex: zIndexTo,
              ease: 'sine.out'
            }
          )
        }
      }
    }
    window.addEventListener('scroll', sticky)
  },
  // update(newValue, oldValue) {
  //   // console.log('update', newValue)
  // },
  unbind() {
    console.log('unbind')
    window.removeEventListener('scroll', sticky)
  }
})
