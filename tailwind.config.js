// Default config: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
module.exports = {
  future: {
    // removeDeprecatedGapUtilities: true,
    purgeLayersByDefault: true,
  },
  purge: [],
  theme: {
    // screens: {
    //   sm: '640px',
    //   md: '768px',
    //   lg: '1024px',
    //   xl: '1280px',
    // },
    extend: {
      colors: {
        primary: '#2F495E',
        secondary: '#FFFFFF',
        'red-mennuni': '#951F1F',
        'red-mennuni-scuro': '#5F0202',
        'gray-mennuni-scuro': '#434343',
      },
      height: {
        '2px': '2px',
      },
    },
    fontFamily: {
      sans: "'IBM Plex Sans Condensed', sans-serif;",
      serif: "'Petrona', serif;",
    },
  },
  variants: {},
  plugins: [
    // Doc: https://tailwindui.com/documentation
    // require("@tailwindcss/ui"),
    // Doc: https://github.com/tailwindcss/typography
    // require('@tailwindcss/typography'),
    // Doc: https://tailwindcss-custom-forms.netlify.app/
    // require("@tailwindcss/custom-forms"),
    require('./plugins/tailwind/grid')({
      columns: 12,
      margin: '1rem',
      // margins: {
      //   default: '0',
      //   // sm: '2rem',
      //   md: 0,
      //   // xl: '3rem',
      // },
      gutter: '2rem',
      maxWidth: '1488px',
    }),
  ],
}
